import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

//ng prime
/* import {ButtonModule} from 'primeng/button';
import { CardModule } from "primeng/card"; */
//import { PrimeNgModule } from './prime-ng/prime-ng.module';
import { SharedModule } from './shared/shared.module';
import { AppRouterModule } from './app-router.module';
import { VentasModule } from './ventas/ventas.module';

//cambiar el local de la app
import localEs from "@angular/common/locales/es-BO";
import localFr from "@angular/common/locales/fr";
import { registerLocaleData } from "@angular/common";
registerLocaleData( localEs )
registerLocaleData( localFr )

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    SharedModule,
    VentasModule,
    //PrimeNgModule
    AppRouterModule
  ],
  //registro de forma global de una variable
  providers: [
    {
      provide: LOCALE_ID, useValue: 'en-AG'
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
