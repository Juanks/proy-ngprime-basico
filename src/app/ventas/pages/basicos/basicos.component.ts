import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-basicos',
  templateUrl: './basicos.component.html',
  styles: [
  ]
})
export class BasicosComponent {
  nombreLower: string = 'lucas'
  nombreUper: string = 'LUCAS'
  nombreCompleto: string = 'lUcaS fiLM'

  fecha: Date = new Date();
}
