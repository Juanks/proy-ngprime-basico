import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-numeros',
  templateUrl: './numeros.component.html',
  styles: [
  ]
})
export class NumerosComponent  {
  ventasNetas: number = 2341234.5567;
  porcentaje : number = 0.5846;
}
